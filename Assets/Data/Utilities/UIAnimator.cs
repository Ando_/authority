﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UIAnimator : MonoBehaviour {

	public static UIAnimator instance;

	public void Awake()
	{
		    if (instance == null)
			    instance = this;
			else
				instance = GameManager.instance.UIManager.animator;
	}

 	public static void LoopThroughObjects(RectTransform rect, float topAmount, float bottomAmount) {
		instance.StartCoroutine(Move_FromBottom(rect, topAmount, bottomAmount));
 	}

	public static void DisplayAlert(RectTransform rect, string message) {
		rect.gameObject.GetComponentInChildren<Text>().text = message;
		instance.StartCoroutine(_DisplayAlert(rect));
	}

	public static void FadeOut(GameObject go) {
		go.SetActive(true);
		instance.StartCoroutine(_FadeOut(go));
	}
	public static void FadeIn(GameObject go) {
		go.SetActive(true);
		instance.StartCoroutine(_FadeIn(go));
	}

	public static void TypeText(Text textobj, string message) {
		textobj.text = "";
		instance.StartCoroutine(_TypeText(textobj, message));
	}

#region Coroutines

	public static IEnumerator _TypeText (Text textObj, string message) {
		foreach (char letter in message.ToCharArray()) {
			textObj.text += letter;

			GameManager.instance.soundManager.PlayClipWithRandomPitch(GameManager.instance.soundManager.typeWriteSound);
			if(Input.GetMouseButtonDown(0))
			{
				textObj.text = message;
				Canvas.ForceUpdateCanvases();
				yield break;
			}

			yield return new WaitForSeconds (0.01f);
		}
	}

	public static IEnumerator DisplayButtons (GameObject[] buttons, float speed)
	{
		while (true)
		{
			foreach (GameObject button in buttons) {

				if(button.GetComponent<CanvasGroup>().alpha != 1)
				{
					button.GetComponent<CanvasGroup>().alpha += Time.deltaTime * speed;
					yield return true;
				}

				if(buttons[buttons.Length-1].GetComponent<CanvasGroup>().alpha == 1)
					yield break;

			}
		}
	}
	public static IEnumerator DisplayButtons (Button[] buttons, float speed)
	{
		while (true)
		{
			foreach (Button button in buttons) {
				if(button.GetComponent<CanvasGroup>().alpha != 1)
				{
					button.GetComponent<CanvasGroup>().alpha += Time.deltaTime * speed;
					yield return true;
				}

				if(buttons[buttons.Length-1].GetComponent<CanvasGroup>().alpha == 1)
					yield break;

			}
		}
	}

	#region Fade Routines
	static IEnumerator _FadeOut(GameObject panel)
	{
		CanvasGroup group = panel.GetComponent<CanvasGroup>();
		while(true)
		{
			if(group.alpha > 0)
			{
				group.alpha -= Time.deltaTime/2;
				yield return true;
			}
			else
			{
				group.alpha = 0;
				panel.SetActive(false);
				//GameManager.instance.StartCoroutine(GameManager.instance.NewClock());
				yield break;
			}
		}
	}

	static IEnumerator _FadeIn(GameObject panel)
	{
		CanvasGroup group = panel.GetComponent<CanvasGroup>();
		group.blocksRaycasts = true;
		while(true)
		{
			if(group.alpha < 1)
			{
				group.alpha += Time.deltaTime/2;
				yield return true;
			}
			else
			{
				group.alpha = 1;
				panel.SetActive(false);
				yield return false;
			}
		}
	}
	#endregion

	#region Alert
	static IEnumerator _DisplayAlert(RectTransform rect)
	{
		float height = rect.rect.height;
		while(true)
		{
			if(rect.anchoredPosition.y < 174f)
			{
				rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, new Vector2(rect.anchoredPosition.x, 175), Time.deltaTime * 2);
				yield return true;
			}
			else
			{
				rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 175);
				yield return new WaitForSeconds(2);
				instance.StartCoroutine(_HideAlert(rect));
				instance.StopCoroutine("_DisplayAlert");
				yield break;
			}
		}
	}

	static IEnumerator _HideAlert(RectTransform rect)
	{
		float height = rect.rect.height;
		while(true)
		{

			if(rect.anchoredPosition.y >= -height + 1)
			{
				rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, new Vector2(0, -height), Time.deltaTime * 2);
				yield return true;
			}
			else
			{
				rect.anchoredPosition = new Vector2(0, -height);
				yield return false;
			}
		}
	}
	#endregion

	static IEnumerator Move_FromBottom(RectTransform rect, float topAmount, float bottomAmount)
	{
		float height = rect.rect.height;
		rect.offsetMax = new Vector2(0, -height);
		rect.offsetMin = new Vector2(0, -height);
		while(true)
		{
			if(rect.offsetMax != new Vector2(rect.offsetMax.x, topAmount))
			{
				rect.offsetMin = Vector2.Lerp(rect.offsetMin, new Vector2(rect.offsetMin.x, topAmount), Time.deltaTime * 2);
				rect.offsetMax = Vector2.Lerp(rect.offsetMax,  new Vector2(rect.offsetMax.x, bottomAmount), Time.deltaTime * 2);
				yield return true;
			}
			else
			{
				rect.offsetMin = new Vector2(rect.offsetMin.x, 0);
				rect.offsetMax = new Vector2(rect.offsetMax.x, 0);
				yield return false;
			}
		}
	}

#endregion

}

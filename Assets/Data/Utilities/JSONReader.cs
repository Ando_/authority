﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to help with organisation.
/// </summary>
[System.Serializable]
public class WalkEvents {
	public List<Event> Events = new List<Event>();
	public Event[] Responses;
}

public static class JSONReader
{
    //Arrays of all JSON data
    private static List<Event> Events = new List<Event>();
    private static WalkEvents walkEvents;
    private static Event[] EventResponses;
    // Enemies
    private static Enemy[] enemies;
    // Items
    private static Weapon[] allWeapons;
    private static Trinket[] allTrinkets;
    private static Armor[] allArmor;


    // Fill arrays with all the data.
    public static void Init()
    {
        // Walk events
        walkEvents = new WalkEvents();
        TextAsset asset = Resources.Load<TextAsset>("WalkEvents");
        walkEvents.Events.AddRange(GetJsonArray<Event>(asset.text));
        asset = Resources.Load<TextAsset>("_WalkEvents");
        walkEvents.Responses = GetJsonArray<Event>(asset.text);

        //asset = Resources.Load<TextAsset>("WorldEvents");
        //worldEvents = GetJsonArray<WorldEvent>(asset.text);


        // Enemies
        asset = Resources.Load<TextAsset>("Enemies");
        enemies = GetJsonArray<Enemy>(asset.text);
        // Items
        asset = Resources.Load<TextAsset>("Weapons");
        allWeapons = GetJsonArray<Weapon>(asset.text);
        asset = Resources.Load<TextAsset>("Armor");
        allArmor = GetJsonArray<Armor>(asset.text);
        asset = Resources.Load<TextAsset>("Trinkets");
        allTrinkets = GetJsonArray<Trinket>(asset.text);
    }

    #region Event Methods

    /// <summary>
    /// Returns a random event.
    /// </summary>
    /// <returns>The random event.</returns>
    public static Event GetRandomEvent()
    {
        int randEvent = Random.Range(0, walkEvents.Events.Count);
        Event newEvent = walkEvents.Events[randEvent];
        walkEvents.Events.RemoveAt(randEvent);
        return newEvent;
    }

    /// <summary>
    /// Returns event from ID
    /// </summary>
    /// <returns>The event by identifier.</returns>
    /// <param name="ID">Identifier.</param>
	public static Event GetEventByID(int ID)
    {
        GameManager.instance.playerData.lastEvent = ID;
        Event newEvent = walkEvents.Events[ID];
        walkEvents.Events.RemoveAt(ID);
        return newEvent;
    }

    /// <summary>
    /// Returns event response from ID
    /// </summary>
    /// <returns>The event response by identifier.</returns>
    /// <param name="ID">Identifier.</param>
	public static Event GetEventResponseByID(int ID)
    {
        Event newEvent = null;
        foreach (Event x in walkEvents.Responses)
        {
            if (x.ID == ID)
            {
                newEvent = x;
            }
        }
        return newEvent;
    }

    #endregion

    #region Item Methods

    public static Item GetRandomItem()
    {
        Item newItem = null;
        switch (Random.Range(1, 3))
        {
            case 1:
                newItem = allWeapons[Random.Range(0, allWeapons.Length)];
                newItem.Type = 0;
                break;

            case 2:
                newItem = allArmor[Random.Range(0, allArmor.Length)];
                newItem.Type = 1;
                break;

            case 3:
                newItem = allTrinkets[Random.Range(0, allTrinkets.Length)];
                newItem.Type = 2;
                break;
        }
        return newItem;
    }

    #endregion

    #region Enemy Methods
    public static Enemy GetEnemyByName(string name)
    {
        Enemy newEntity = null;
        Debug.Log(enemies[0]);

        foreach (Enemy x in enemies)
        {

            Debug.Log(x);

            if (x.Name.Equals(name))
            {
                newEntity = x;
            }
        }
        return newEntity;
    }

    #endregion

    #region Weapon Methods
    // Weapon methods
    public static Weapon WeaponByID(int ID)
    {
        Weapon thisWeapon = null;

        foreach (Weapon x in allWeapons)
        {
            if (x.ID == ID)
            {
                thisWeapon = x;
            }
        }

        Debug.Log("Returned weapon: " + thisWeapon.Name);
        return thisWeapon;
    }
    public static Weapon WeaponByName(string name)
    {
        Weapon thisWeapon = null;

        foreach (Weapon x in allWeapons)
        {
            if (x.Name == name)
            {
                thisWeapon = x;
            }
        }

        Debug.Log("Returned weapon: " + thisWeapon.Name);
        return thisWeapon;
    }
    #endregion

    #region Armor Methods
    public static Armor ArmorByID(int ID)
    {
        Armor thisArmor = null;

        foreach (Armor x in allArmor)
        {
            if (x.ID == ID)
            {
                thisArmor = x;
            }
        }
        return thisArmor;
    }

    public static Armor ArmorByName(string name)
    {
        Armor thisArmor = null;

        foreach (Armor x in allArmor)
        {
            if (x.Name == name)
            {
                thisArmor = x;
            }
        }
        return thisArmor;
    }
    #endregion

    #region Trinket Methods
    public static Trinket TrinketByID(int ID)
    {
        Trinket thisTrinket = null;

        foreach (Trinket x in allTrinkets)
        {
            if (x.ID == ID)
            {
                thisTrinket = x;
            }
        }

        return thisTrinket;
    }

    public static Trinket TrinketByName(string name)
    {
        Trinket thisTrinket = null;

        foreach (Trinket x in allTrinkets)
        {
            if (x.Name == name)
            {
                thisTrinket = x;
            }
        }

        return thisTrinket;
    }
    #endregion

    #region Helper Methods
    //Functions that will return an array of all the JSON data
    public static T[] GetJsonArray<T>(string json)
    {
        string newJson = "{ \"array\": " + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] array;
    }
    #endregion
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData {

	public string playerName;
	public int cash;
	public int food;

	public int seed;
	public int lastEvent;
	public int dangerLevel;

	// Progression data.
	[Header("Progression")]
	public int curRoom;
	public int maxRooms;
	public int curFloor;
	// Stat data.
	[Header("Stats")]
	public int sanity;
	public int health;
	public int defence;
	public int strength;
	public int magic;
	// Inventory data.
	[Header("Inventory")]
	public Weapon weapon;
	public Armor armor;
	public Trinket trinket;

}
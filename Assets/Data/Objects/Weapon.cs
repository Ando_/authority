﻿
// Weapons have a damage value that is used in combat and can have multiple changes to variables.
[System.Serializable]
public class Weapon : Item {

	public int Damage;
	public VariableChange[] Changes;

}

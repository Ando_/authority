﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Event
{
    public int ID;
    public string Text;
    public Option[] Options;
    public VariableChange[] EventVariablesChanges;
}

[System.Serializable]
public struct OptionDisplayCheck
{
    public string VariableToCheck;
    public bool Show;
    public string Value;
}

[System.Serializable]
public struct OptionDisableCheck
{
    public string VariableToCheck;
    public string Value;
}

[System.Serializable]
public struct VariableChange
{
    public string VariableToChange;
    public string Value;
}

[System.Serializable]
public struct Option
{
    public string OptionText;
    public OptionType OptionType;
    public OptionDisplayCheck DisplayCheck;
    public OptionDisableCheck DisableCheck;
    public VariableChange[] Changes;
    public int ToEvent;
}

public enum OptionType
{
    ToEncounter,
    ToBattle,
    ToStore
}


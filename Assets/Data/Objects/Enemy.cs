﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Enemy
{
    public int ID;
    public string Name;
    public int Health;
    public Attacks[] Attacks;
    public int AttackCooldown;
}

[System.Serializable]
public struct Attacks
{
    public string Description;
    public int Damage;
}

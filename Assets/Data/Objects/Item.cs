﻿[System.Serializable]
public class Item
{
    public int ID;
    public string Name;
    public int Cost;
    public int Type;
}

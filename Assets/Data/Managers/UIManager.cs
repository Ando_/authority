﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Text.RegularExpressions;

[System.Serializable]
public class UIManager : MonoBehaviour {

	[Header("Prefab References")]
	public GameObject buttonPrefabs;
	public GameObject actionButtonPrefab;
	public GameObject statPrefab;
	public GameObject enemyStatsPrefab;
    public GameObject combatLogPrefab;

	[Header("Component References")]
	public UIAnimator animator;

	[Header("Panels")]
	public Transform sleepPanel;

	[Header("Home Panel")]
	public Image distanceProgressBar;
	public Transform homePanel;
	[Space(5)]
	public Button workButton;
	public Button sleepButton;
	public Transform logParent;

	[Header("Event Panel")]
	public Transform eventPanel;
	[Space(5)]
	public Text eventText;
	public Transform eventButtonParent;
	public RectTransform eventStatChangeLayout;
	public RectTransform eventStatParent;

	[Header("Store Panel")]
	public Transform shopPanel;
	public Transform buyPanel;
	public Transform sellPanel;
	[Space(5)]
	public Transform buyParent;
	public Transform sellParent;
	public Text shopText;

	[Header("Failstate Panel")]
	[Space(5)]
	public Transform failstatePanel;
	public Text failstate_reasonText;
	public Button returnButton;

	[Header("Bottom Menu")]
	public Button[] menuButtons;
	public Transform statPanel;
	public Transform actionPanel;
	public Transform inventoryPanel;
	public Transform levelParent;

	[Header("Stat Panel")]
	public Text healthText;
	public Text damageText;
	public Text defenceText;
	public Text magicText;
	public Text sanityText;

	[Header("Inventory UI")]
	public Text weaponText;
	public Text clothesText;
	public Text trinketText;

	[Header("Battle UI")]
	public Transform battlePanel;
    public Transform combatLogParent;

	public Button attackButton;
	public Button FleeButton;
    public Image attackCooldownImage;
	public Text enemyNameText;
	public Text attackText;
	public Image enemyHealthBar;
	public Image enemyAttackBar;
    [Space(10)]
    public Transform endPanel;
    public Button endBattleButton;



	[Header("PopUp Panel")]
	public RectTransform popupPanel;
	public Text popupText;

	#region UI Update Methods
	/*
	Update stats UI elements.
	*/
	public void UpdateStatUI () {
		healthText.text = GameManager.instance.playerData.health.ToString();
		damageText.text = GameManager.instance.playerData.strength.ToString();
		defenceText.text = GameManager.instance.playerData.defence.ToString();
		magicText.text = GameManager.instance.playerData.magic.ToString();
		sanityText.text = GameManager.instance.playerData.sanity.ToString();
	}

	/*
	Update the inventory name with the needed values.
	*/
	public void UpdateInventoryUI () {
        if(GameManager.instance.playerData.weapon != null)
		{
            weaponText.color = Color.white;
			weaponText.text = GameManager.instance.playerData.weapon.Name + " (" + GameManager.instance.playerData.weapon.Damage + ")";
		}
        else
        {
            weaponText.color = Color.gray;
            weaponText.text = "Weapon";
        }
        if(GameManager.instance.playerData.armor != null)
		{
            clothesText.color = Color.white;
			clothesText.text = GameManager.instance.playerData.armor.Name;
		}
        else
        {
            clothesText.color = Color.gray;
            clothesText.text = "Armor";
        }
        if(GameManager.instance.playerData.trinket != null)
		{
            trinketText.color = Color.white;
			trinketText.text = GameManager.instance.playerData.trinket.Name;
		}
        else
        {
            trinketText.color = Color.gray;
            trinketText.text = "Trinket";
        }
	}

	/*
	This methods checks the players current progress through a floor and then Updates the UI accordingly. It does this by using a for loop to check the
	levelParent's children against varaibles like playerdata.maxrooms and makes them visible and a different color to represent the players progress.
	*/
	public void UpdateLevelUI () {
		Image[] images = levelParent.GetComponentsInChildren<Image>(true);
		for (int i = 1; i < images.Length; i++)
		{

			if(i <= GameManager.instance.playerData.maxRooms)
			{
				images[i-1].gameObject.SetActive(true);

				if(i <= GameManager.instance.playerData.curRoom)
				{
					images[i-1].GetComponentInChildren<Image>().color = Color.black;
				}
				else
				{
					images[i-1].GetComponentInChildren<Image>().color = Color.gray;
				}
			}
			else
			{
				images[i].gameObject.SetActive(false);
			}
		}
	}
	#endregion

	#region DisplayUI Methods

	// Event UI
	public void DisplayEventUI (Event _event) {
		eventText.text = "";
		StartCoroutine(NewCreateEventUI(_event));
	}
	public IEnumerator NewCreateEventUI (Event _event)
	{
		eventStatChangeLayout.gameObject.SetActive(false);

		foreach(Transform child in eventButtonParent.GetComponentsInChildren<Transform>())
		{
			if (child.gameObject.GetInstanceID() != eventButtonParent.gameObject.GetInstanceID())
				Destroy(child.gameObject);
		}

        string eventString = _event.Text;

        Regex regex = new Regex(@"(?<=\[)(.*?)(?=\])");
        foreach (Match match in regex.Matches(_event.Text))
        {
            CaptureCollection captures = match.Captures;
            Debug.Log(captures[0].Value);
            GameManager.instance.curEnemy = JSONReader.GetEnemyByName(captures[0].Value);
            eventString = _event.Text.Replace("[" + GameManager.instance.curEnemy.Name + "]", GameManager.instance.curEnemy.Name);
        }

		eventPanel.gameObject.SetActive(true);
		eventPanel.gameObject.GetComponent<CanvasGroup>().alpha = 1;

		if(_event.EventVariablesChanges != null)
			ChangeItems(_event.EventVariablesChanges);
        
		yield return StartCoroutine(UIAnimator._TypeText(eventText, eventString));

		// Check stats for event.
		if(_event.EventVariablesChanges != null)
		{
			foreach(Transform child in eventStatParent.GetComponentsInChildren<Transform>())
			{
				if (child.gameObject.GetInstanceID() != eventStatParent.gameObject.GetInstanceID())
					Destroy(child.gameObject);
			}

            StartCoroutine(ChangeVariables(_event.EventVariablesChanges));
		}
		//Canvas.ForceUpdateCanvases();

		// Display actions the player can take.
		List<GameObject> buttons = new List<GameObject>();
		for(int i = 0; i < _event.Options.Length; i++)
		{
            // Check if the option has a display check and caluculate if the option can be displayed.
            bool createButton = true;
            if(_event.Options[i].DisplayCheck.VariableToCheck != null)
            {
                int playerValue = (int)GameManager.instance.playerData.GetType().GetField(_event.Options[i].DisplayCheck.VariableToCheck).GetValue(GameManager.instance.playerData);
                var testValue = int.Parse(_event.Options[i].DisplayCheck.Value);

                // If LessThan is true then check if playerValue is less then testValue.
                if(playerValue < testValue)
                {
                    if(_event.Options[i].DisplayCheck.Show)
                    {
                        createButton = true;
                        yield return null;
                    }
                    else
                    {
                        createButton = false;
                        yield return null;
                    }
                }
            }
                
            if(createButton)
            {
                GameObject newButton = Instantiate(actionButtonPrefab);
                newButton.GetComponentInChildren<Text>().text = _event.Options[i].OptionText;

                if (_event.Options[i].OptionType == OptionType.ToBattle)
                    newButton.GetComponentInChildren<Image>().color = Color.red;

                Debug.Log(i);
                int num = i;
                newButton.GetComponent<Button>().onClick.AddListener(delegate { EventButtonPressed(num, _event); });
                buttons.Add(newButton);
                newButton.transform.SetParent(eventButtonParent, false);
            }
		}
		yield return StartCoroutine(UIAnimator.DisplayButtons(buttons.ToArray(), 0.1f));

		yield break;
	}

	public void ChangeItems (VariableChange[] changes, bool negative = false)
	{
		for(int i = 0; i < changes.Length; i++)
		{
			if(changes[i].VariableToChange == "Weapon")
			{
				GameManager.instance.EquipItem(JSONReader.WeaponByName(changes[i].Value));
				return;
			}
            if (changes[i].VariableToChange == "Armor")
            {
                GameManager.instance.EquipItem(JSONReader.ArmorByName(changes[i].Value));
                return;
            }
            if (changes[i].VariableToChange == "Trinket")
            {
                GameManager.instance.EquipItem(JSONReader.TrinketByName(changes[i].Value));
                return;
            }
		}
	}

    public IEnumerator ChangeVariables (VariableChange[] changes, bool negative = false)
	{
		for(int i = 0; i < changes.Length; i++)
		{
            if(changes[i].VariableToChange == "Weapon" || changes[i].VariableToChange == "Armor" || changes[i].VariableToChange == "Trinket")
			{
				yield break;
			}

			Type type = GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).FieldType;

			var value = Convert.ChangeType(changes[i].Value, Type.GetTypeCode(type));

			// Perform different things for different value types
			switch(Type.GetTypeCode(type))
			{
				case TypeCode.Int32:
				value =  (int)value + (int)GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).GetValue(GameManager.instance.playerData);
				if(negative)
				{
					value = -(int)value;
				}
				break;
			}

			GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).SetValue(GameManager.instance.playerData, value);

			GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).SetValue(GameManager.instance.playerData, value);
			GameObject newStat = Instantiate(statPrefab, eventStatParent);
			Text[] texts = newStat.GetComponentsInChildren<Text>();
			texts[0].text = changes[i].Value.ToString();
			texts[1].text = changes[i].VariableToChange.ToUpper();
		}
		eventStatChangeLayout.gameObject.SetActive(true);

		// Reload specfic layout elements.
		LayoutRebuilder.ForceRebuildLayoutImmediate(eventStatParent);
		LayoutRebuilder.ForceRebuildLayoutImmediate(eventStatChangeLayout);
		LayoutRebuilder.ForceRebuildLayoutImmediate(eventStatChangeLayout);

        GameManager.instance.UIManager.UpdateStatUI();
        yield return new WaitForSeconds(1);
        GameManager.instance.CheckStats();

        yield break;
	}

	public void DisplayBattleUI (Event _event) {

		// Get a list of enemies from all present within the text.
		// Detect enemy within the event text

        // Set up player UI.
        attackText.text = "D6 + " + GameManager.instance.playerData.strength + " + " + GameManager.instance.playerData.weapon.Damage;
        attackButton.onClick.AddListener(delegate { GameManager.instance.PlayerAttack(); });
        endBattleButton.onClick.AddListener(delegate { EndBattleButtonPressed(); });

		// Create UI for the enemies.
        enemyNameText.text = GameManager.instance.curEnemy.Name;

		eventPanel.gameObject.SetActive(false);
		battlePanel.gameObject.SetActive(true);

		GameManager.instance.BeginBattle();

	}

    public void CreateBattleLog (string message, Color32 color) 
    {
        GameObject newLog = Instantiate(combatLogPrefab, combatLogParent);
        newLog.GetComponent<Text>().color = color;
        newLog.GetComponent<Text>().text = message;
    }

	// Shop UI

	public void CreateShopUI ()
	{
		shopPanel.gameObject.SetActive(true);
		StopAllCoroutines();
		// Clear buttons
		foreach(Transform child in eventButtonParent.GetComponentsInChildren<Transform>())
		{
			if (child.gameObject.GetInstanceID() != eventButtonParent.gameObject.GetInstanceID())
				Destroy(child.gameObject);
		}
		foreach(Transform child in buyParent.GetComponentsInChildren<Transform>())
		{
			if (child.gameObject.GetInstanceID() != buyParent.gameObject.GetInstanceID())
				Destroy(child.gameObject);
		}
		foreach(Transform child in sellParent.GetComponentsInChildren<Transform>())
		{
			if (child.gameObject.GetInstanceID() != sellParent.gameObject.GetInstanceID())
				Destroy(child.gameObject);
		}

		List<GameObject> buttons = new List<GameObject>();
		for(int i = 0; i < 3; i++)
		{
			GameObject newButton = Instantiate(actionButtonPrefab);

			switch (i)
			{
				case 0:
				newButton.GetComponentInChildren<Text>().text = "Buy";
				break;
				case 1:
				newButton.GetComponentInChildren<Text>().text = "Sell";
				break;
				case 2:
				newButton.GetComponentInChildren<Text>().text = "Leave";
				break;
			}

			newButton.GetComponent<Button>().onClick.AddListener( delegate { ShopNavButtonPressed(newButton.GetComponent<Button>()); } );
			buttons.Add(newButton);
			newButton.transform.SetParent(eventButtonParent, false);
		}

		// Generate Purchaseable items.
		for(int i = 0; i < UnityEngine.Random.Range(2, 5); i++)
		{
			Item item = JSONReader.GetRandomItem();
            GameManager.instance.shopItems.Add(item);
			GameObject newButton = Instantiate(buttonPrefabs);
			newButton.GetComponent<CanvasGroup>().alpha = 1;
			newButton.GetComponentInChildren<Text>().text = item.Name + " : $" + item.Cost;
            newButton.GetComponent<Button>().onClick.AddListener( delegate { ShopBuyButtonPressed(newButton); } );
			newButton.transform.SetParent(buyParent, false);
		}

		CreateShopUI_Buy();
	}

	public void CreateShopUI_Buy ()
	{
		buyPanel.gameObject.SetActive(true);
		Button[] buttons = buyParent.GetComponentsInChildren<Button>();
		//StartCoroutine(UIAnimator.DisplayButtons(buttons, 3f));
	}

	public void CreatePopupUI (Weapon replaceItem) {
		popupPanel.gameObject.SetActive(true);
		popupText.text = "Replace \n" + GameManager.instance.playerData.weapon + "\nwith\n" + replaceItem.Name;

	}

	public void DisplayFailStateUI (string message)
	{
		failstatePanel.gameObject.SetActive(true);
		failstate_reasonText.text = message;
	}

	#endregion

	#region ButtonPressEvents

    public void QuitButtonPressed () {
        GameManager.instance.ChangeScene("Menu");
    }

	public void MenuButtonPressed (Button button) {
        GameManager.instance.soundManager.PlayClip(GameManager.instance.soundManager.buttonPressSound);

		// Check buttons and highlight the selected one.
		foreach (Button _button in menuButtons)
		{
			if(button.transform.GetInstanceID() != _button.transform.GetInstanceID())
			{
				_button.GetComponentInChildren<Image>().color = Color.grey;
			}
			else
			{
				_button.GetComponentInChildren<Image>().color = Color.white;
			}
		}

		switch (button.GetComponentInChildren<Text>().text)
		{
			case "ACTIONS":
			actionPanel.gameObject.SetActive(true);
			inventoryPanel.gameObject.SetActive(false);
			statPanel.gameObject.SetActive(false);
			break;

			case "INVENTORY":
			actionPanel.gameObject.SetActive(false);
			inventoryPanel.gameObject.SetActive(true);
			statPanel.gameObject.SetActive(false);
			break;

			case "STATS":
			actionPanel.gameObject.SetActive(false);
			inventoryPanel.gameObject.SetActive(false);
			statPanel.gameObject.SetActive(true);
			break;
		}
	}

	public void EventButtonPressed(int index, Event _event)
	{
        GameManager.instance.soundManager.PlayClip(GameManager.instance.soundManager.buttonPressSound);

		StopAllCoroutines();
        Debug.Log(index);
		// If events to display the correct UI
		if(_event.Options[index].OptionType == OptionType.ToBattle)
		{
			DisplayBattleUI(_event);
			return;
		}
		if(_event.Options[index].OptionType == OptionType.ToStore)
		{
			//LocationText.text = "Merchant";
			CreateShopUI();
			return;
		}

		int nextEvent = _event.Options[index].ToEvent;

		// If -1 will conclude the event and progress the player forward.
		if(nextEvent == -1)
		{
			GameManager.instance.NewEvent();
			return;
		}
		Option thisOption = _event.Options[index];
		// Change variables if needed.
		if(_event.Options[index].Changes != null)
		{
			for(int i = 0; i < thisOption.Changes.Length; i++)
			{
				Type type = GameManager.instance.GetType().GetField(thisOption.Changes[i].VariableToChange).FieldType;
				Debug.Log("Change code for: " + type + " = " + Type.GetTypeCode(type));

				var value = Convert.ChangeType(thisOption.Changes[i].Value, Type.GetTypeCode(type));

				// Perform different things for different value types
				switch(Type.GetTypeCode(type))
				{
					case TypeCode.Int32:
					value =  (int)value + (int)GameManager.instance.GetType().GetField(thisOption.Changes[i].VariableToChange).GetValue(GameManager.instance);
					break;

				}

				GameManager.instance.GetType().GetField(thisOption.Changes[i].VariableToChange).SetValue(GameManager.instance, value);
			}
		}

		DisplayEventUI(JSONReader.GetEventResponseByID(nextEvent));
	}

	// Shop Button Events

	public void ShopNavButtonPressed (Button button) {

		switch (button.GetComponentInChildren<Text>().text)
		{
			case "Buy":
			buyPanel.gameObject.SetActive(true);
			sellPanel.gameObject.SetActive(false);
			break;

			case "Sell":
			buyPanel.gameObject.SetActive(false);
			sellPanel.gameObject.SetActive(true);
			break;

			case "Leave":
			shopPanel.gameObject.SetActive(false);
			eventPanel.gameObject.SetActive(true);
			GameManager.instance.NewEvent();
			break;
		}
	}
    public void ShopBuyButtonPressed(GameObject button) {
        if(GameManager.instance.playerData.magic >= GameManager.instance.shopItems[button.transform.GetSiblingIndex()].Cost)
        {
            Debug.Log("BUY");
        }
	}


	public void LeaveButtonPressed()
	{

	}

    public void EndBattleButtonPressed ()
    {
        endPanel.gameObject.SetActive(false);
        battlePanel.gameObject.SetActive(false);
        GameManager.instance.NewEvent();
    }

	#endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	public Button[] saveFileButtons;

	[Header("CreateFilePanel")]
	public Transform createPlayerPanel;
	public InputField playerNameField;

	public void Awake () {
		SaveLoadManager.Load();

		for (int i = 0; i < 3; i++)
		{
			if(SaveLoadManager.savedGames[i] == null)
			{
				saveFileButtons[i].GetComponentInChildren<Text>().text = "New file";
			}
			else
			{
				saveFileButtons[i].GetComponentInChildren<Text>().text = SaveLoadManager.savedGames[i].playerName;
			}
		}
	}

	#region Save functionaility
	// When a file buttton is pressed will check the text on it to see if it needs to direct to create a new file or load an existing one.
	public void SaveFileButtonPressed (Button button) {
        GameManager.instance.soundManager.PlayClip(GameManager.instance.soundManager.buttonPressSound);

		Debug.Log(button.transform.GetSiblingIndex());
		// Set saveIndex to sibling index of the button thats pressed.
		GameManager.instance.saveIndex = button.transform.GetSiblingIndex();

		// Check if the program needs to create a new save or load an existing one.
		if(button.GetComponentInChildren<Text>().text == "New file")
		{
			createPlayerPanel.gameObject.SetActive(true);
		}
		else
		{
			LoadExistingGame();
		}
	}

	public void DeleteSaveButtonPressed (int index) {
		saveFileButtons[index].GetComponentInChildren<Text>().text = "New file";
		SaveLoadManager.DeleteSave(index);
	}

	// Creates a new save file using the text from the input field.
	public void CreateNewGame () {
        GameManager.instance.soundManager.PlayClip(GameManager.instance.soundManager.buttonPressSound);

		SaveData newSave = new SaveData();
		// Error checking
		if(playerNameField.text != "")
		{
			newSave.playerName = playerNameField.text;
		}
		else
		{
			Debug.Log("Name cannot be empty");
		}
		// Set inital values of new save.
		newSave.seed = (int)System.DateTime.Now.Ticks;
		newSave.health = 20;
		newSave.strength = 1;
		newSave.sanity = 100;
		newSave.defence = 1;
		newSave.magic = 0;
		newSave.curFloor  = 1;
		// Set as current player data and save to specified index.
		GameManager.instance.playerData = newSave;
		//Equip default items.
		GameManager.instance.EquipItem(JSONReader.WeaponByID(0));
		GameManager.instance.EquipItem(JSONReader.ArmorByID(0));

		SaveLoadManager.Save(GameManager.instance.saveIndex);
		// Load main scene.
		SceneManager.LoadScene("Main");
	}

	// Load an existing game and moves to the "Main" scene.
	public void LoadExistingGame () {
		GameManager.instance.playerData = SaveLoadManager.savedGames[GameManager.instance.saveIndex];
		SceneManager.LoadScene(1);
	}
	#endregion

	#region Options

	public void OpenOptions () {

	}

	#endregion

	public void ExitGame () {
		Application.Quit();
	}
}

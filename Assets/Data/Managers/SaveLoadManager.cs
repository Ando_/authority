﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public static class SaveLoadManager {

    public static SaveData[] savedGames = new SaveData[3];


	public static void Save (int index) {
    	savedGames[index] = GameManager.instance.playerData;
    	BinaryFormatter bf = new BinaryFormatter();
    	FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
    	bf.Serialize(file, SaveLoadManager.savedGames);
    	file.Close();
	}

	public static void Load () {
		if(File.Exists(Application.persistentDataPath + "/savedGames.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			SaveLoadManager.savedGames = (SaveData[])bf.Deserialize(file);
			file.Close();
		}
	}

	public static void DeleteSave (int index) {
		if(File.Exists(Application.persistentDataPath + "/savedGames.gd")) {
			savedGames[index] = null;
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
			bf.Serialize(file, SaveLoadManager.savedGames);
			file.Close();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System;
using UnityEngine.SceneManagement;


// Singleton class that can be accessed anywhere within the codebase.
public class GameManager : MonoBehaviour {
    
	public static GameManager instance;

	[Header("Save")]
	public int saveIndex;
	public SaveData playerData;

	[Header("Debug")]
	public float timeScale;
	public UIManager UIManager;
	public SoundManager soundManager;

	[Header("Battle")]
	public Enemy curEnemy;
    public float curHealth;
    public bool canAttack;

    [Header("Shop")]
    public List<Item> shopItems = new List<Item>();

    public int dangerLevel;

	// Set up signleton
	void Awake () {
		//Ensure singleton
        if (instance == null)
		{
			//if not, set instance to this
			instance = this;
		}
		//If instance already exists and it's not this:
		else if (instance != this)

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
		SceneManager.activeSceneChanged += instance.SceneChanged;
		soundManager = gameObject.GetComponent<SoundManager>();
		// Initilise JSON Reader to get all of the initial data.
		JSONReader.Init();
	}

    #region Navigation Functionaility
    // Called when the scene is changed to perform set up specifc to the scene.
    public void SceneChanged (Scene fromScene, Scene toScene)
	{
		switch(toScene.name)
		{
			case "Main":
			instance.UIManager = GameObject.Find("LevelManager").GetComponent<UIManager>();
			instance.UIManager.animator = instance.gameObject.AddComponent<UIAnimator>();

			UIAnimator.FadeOut(UIManager.sleepPanel.gameObject);
			UIManager.UpdateStatUI();
			UIManager.UpdateInventoryUI();

			// If new file create the event at 0 or load last room.
			if(instance.playerData.maxRooms == 0)
			{
				UIManager.DisplayEventUI(JSONReader.GetEventByID(0));
			}
			else
			{
				UIManager.DisplayEventUI(JSONReader.GetEventByID(instance.playerData.lastEvent));
				UIManager.UpdateLevelUI();
			}

			UIManager.returnButton.GetComponent<Button>().onClick.AddListener( delegate { instance.ChangeScene("Menu"); } );

			break;
		}
	}

    /// <summary>
    /// Changes the current scene.
    /// </summary>
    /// <param name="sceneName">Scene name.</param>
	public void ChangeScene (string sceneName) {
		SceneManager.LoadScene(sceneName);
	}

    #endregion

    /// <summary>
    /// Generates a new floor with a new maxRooms values and resets the players current room.
    /// </summary>
    public void NewFloor () {
		instance.playerData.curFloor += 1;
		// Generate new floor values.
		int amountOfRooms = UnityEngine.Random.Range(3,5) + instance.playerData.curFloor;
		// Clamp to ensure reasonable value.
		int amountOfRoomsFinal = Mathf.Clamp(amountOfRooms, 0, 15);

		instance.playerData.maxRooms = amountOfRoomsFinal;
		instance.playerData.curRoom = 0;
		SaveLoadManager.Save(GameManager.instance.saveIndex);
		UIManager.UpdateLevelUI();

		NewEvent();
	}

    /// <summary>
    /// Creates a new event depending on the players progress.
    /// </summary>
	public void NewEvent () {
		instance.playerData.curRoom += 1;
		StopAllCoroutines();
		// Check if last level reached.
		// TODO: Spawn an end room.
		if (instance.playerData.curRoom == instance.playerData.maxRooms)
		{
		}

		// If past the max rooms generate a new floor.
		if(instance.playerData.curRoom > instance.playerData.maxRooms)
		{
			NewFloor();
			return;
		}

		UIManager.UpdateLevelUI();

		Event newEvent = null;
		newEvent = JSONReader.GetRandomEvent();
		// = JSONReader.GetEventByID(8);
		UIManager.DisplayEventUI(newEvent);

		playerData.lastEvent = newEvent.ID;
		SaveLoadManager.Save(GameManager.instance.saveIndex);
	}

	public void Death(string reason)
	{
		UIManager.DisplayFailStateUI(reason);
	}

	public void CheckStats () {
		// Health Check
		if(GameManager.instance.playerData.health <= 0)
		{
			StopAllCoroutines();
			Death("Your body loses life and you fall to the ground, another body for the spire");
		}

        if (GameManager.instance.playerData.sanity <= 0)
        {
            StopAllCoroutines();
            Death("Your mind has become to corrupt. The husk that once was your body wanders the halls of the spire forever as an undead.");
        }
	}

	#region Inventory functionaility
	public void ConfirmItemEquip ()
	{

	}
	/*
	Equip Item methods: Theses methods will take th
	*/
	public void EquipItem (Trinket trinket) {

		Trinket lastTrinket = null;
		if(GameManager.instance.playerData.trinket != null)
		{
			lastTrinket = instance.playerData.trinket;
			ChangeVariables(lastTrinket.Changes, true);
		}

		ChangeVariables(trinket.Changes);
		GameManager.instance.playerData.trinket = trinket;

		if(SceneManager.GetActiveScene().name == "Main")
		{
			UIAnimator.DisplayAlert(UIManager.popupPanel, GameManager.instance.playerData.trinket.Name + " Replaced with: " + trinket.Name);
			UIManager.UpdateInventoryUI();
			UIManager.UpdateStatUI();
		}
	}
	public void EquipItem (Weapon weapon) {

		Weapon lastWeapon = null;
		if(GameManager.instance.playerData.weapon != null)
		{
			lastWeapon = instance.playerData.weapon;
			ChangeVariables(lastWeapon.Changes, true);
		}

		ChangeVariables(weapon.Changes);
		instance.playerData.weapon = weapon;

		if(SceneManager.GetActiveScene().name == "Main")
		{
			UIAnimator.DisplayAlert(UIManager.popupPanel, lastWeapon.Name + " (" + lastWeapon.Damage + ")" + " Replaced with: " + weapon.Name + " (" + weapon.Damage + ")");
			UIManager.UpdateInventoryUI();
			UIManager.UpdateStatUI();
		}

	}
	public void EquipItem (Armor armor) {

		Armor lastArmor = null;
		if(GameManager.instance.playerData.armor != null)
		{
			lastArmor = instance.playerData.armor;
			ChangeVariables(lastArmor.Changes, true);
		}

		ChangeVariables(armor.Changes);
		GameManager.instance.playerData.armor = armor;

		if(SceneManager.GetActiveScene().name == "Main")
		{
			UIAnimator.DisplayAlert(UIManager.popupPanel, lastArmor.Name + " Replaced with: " + armor.Name);
			UIManager.UpdateInventoryUI();
			UIManager.UpdateStatUI();
		}
	}

	// Summary of method
	public void ChangeVariables (VariableChange[] changes, bool negative = false)
	{
		for(int i = 0; i < changes.Length; i++)
		{
			//
			if(changes[i].VariableToChange == "Weapon")
			{
				EquipItem(JSONReader.WeaponByName(changes[i].Value));
				return;
			}

			Type type = GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).FieldType;

			var value = Convert.ChangeType(changes[i].Value, Type.GetTypeCode(type));

			// Perform different things for different value types
			switch(Type.GetTypeCode(type))
			{
				case TypeCode.Int32:
				if(negative)
				{
					value = -(int)value;
				}
				value =  (int)value + (int)GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).GetValue(GameManager.instance.playerData);
				break;
			}
			GameManager.instance.playerData.GetType().GetField(changes[i].VariableToChange).SetValue(GameManager.instance.playerData, value);
		}
	}

	#endregion

	#region Battle Functionailty
	public void BeginBattle () {
        curHealth = curEnemy.Health;
        canAttack = true;
		StartCoroutine(_EnemyAttack());
	}

    public void EndBattle (string condition) {
        switch (condition)
        {
            case "Victory":
                Debug.Log("ENEMY DEAD");
                curEnemy = null;
                UIManager.endPanel.gameObject.SetActive(true);

                break;
        }
    }

	public void EnemyAttack () {

        //Select a random Attack, damage the play and create a combat log instance.
        int rand = UnityEngine.Random.Range(0, curEnemy.Attacks.Length);
        for (int i = 0; i < curEnemy.Attacks.Length; i++)
        {
            instance.playerData.health -= curEnemy.Attacks[rand].Damage;
            Debug.Log(curEnemy.Attacks[rand].Description);
            UIManager.CreateBattleLog(curEnemy.Attacks[rand].Description, Color.red);
        }
		UIManager.UpdateStatUI();
		instance.CheckStats();
	}

	public void PlayerAttack () {
        if (canAttack)
        {
            // TODO: DAMAGE CALC
            UIManager.CreateBattleLog("You hit the " + curEnemy.Name + " with your " + playerData.weapon.Name + " for " + playerData.weapon.Damage + " Damage", Color.black);

            canAttack = false;
            StartCoroutine(_AttackCoolDown());

            curHealth -= instance.playerData.weapon.Damage;
            var percent = curHealth / (float)instance.curEnemy.Health;
            UIManager.enemyHealthBar.transform.localScale = new Vector3(percent, UIManager.enemyHealthBar.transform.localScale.y, UIManager.enemyHealthBar.transform.localScale.z);

            if (curHealth <= 0)
            {
                StopAllCoroutines();
                EndBattle("Victory");
            }
        }
	}

    public void Flee () {
        int randInt = UnityEngine.Random.Range(0, 100);

        if(randInt > 50)
        {
            Debug.Log("FLEE");
        }
    }

	public IEnumerator _EnemyAttack () {
		int maxTime = 3;
		float activeTime = 0;
		while (true)
		{
			if(activeTime <= maxTime)
			{
			    activeTime += Time.deltaTime;
    		 	var percent = activeTime / maxTime;
     			UIManager.enemyAttackBar.transform.localScale = new Vector3(percent, UIManager.enemyAttackBar.transform.localScale.y, UIManager.enemyAttackBar.transform.localScale.z);
				yield return true;
			}
			else
			{
				activeTime = 0;
				EnemyAttack();
				yield return true;
			}
		}
	}

    public IEnumerator _AttackCoolDown () {
        float maxTime = 3;
        float activeTime = 0;
        while (true)
        {
            if(activeTime <= maxTime)
            {

                activeTime += Time.deltaTime;
                var percent = activeTime / maxTime;
                UIManager.attackCooldownImage.transform.localScale = new Vector3(percent, UIManager.attackCooldownImage.transform.localScale.y, UIManager.attackCooldownImage.transform.localScale.z);
                yield return true;
            }
            else
            {
                canAttack = true;
                yield break;
            }
        }
    }
	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioClip typeWriteSound;
    public AudioClip buttonPressSound;
	// Use this for initialization
	public void PlayClip (AudioClip clip) {
		AudioSource newSource = gameObject.AddComponent<AudioSource>();
		newSource.PlayOneShot(clip);
		Destroy(newSource, clip.length);
	}

	public void PlayClipWithRandomPitch (AudioClip clip) {
		AudioSource newSource = gameObject.AddComponent<AudioSource>();
		newSource.pitch = Random.Range(0.8f, 1.1f);
		newSource.PlayOneShot(clip);
		Destroy(newSource, clip.length);
	}
}

# AUTHORITY #

A text based adventure about surviving in a dsytopian society.

### Description ###

A text based adventure game.
v1

### Directory Layout ###

* Scene - Contains all scenes.
* Sounds - Contains all sound.
* Images - Contains all PNG files.
	* RawSources - Contains all PSD files.
* Resources - Contains all JSON files.
* Data - Contains all scripts.
	* Managers - Contains all manager scripts.
	* Objects - Contains all objects for the JSON data.
	* Utilities - Contains JSONReader and UIAnimater.
* Fonts - Contains all fonts.
* Prefabs - Contains all prefabs.

### How do I get set up? ###

Simply download then open with unity then build to one of the supported platforms. Currently set to Android but can be switched to apple if needed.

### How do I run it on my phone?###

Ensure your phone is plugged in and allows USB debugging.

#### Android ####

You must switch the build platform by going to: 

* Go to File/BuildSettings.
* select the platform from the list.
* press the switch platform button in the bottom left.

#### iOS ####

Follow instructions above.


Once the correct platform has been chosen go to File/Build & Run and after the project has finished building it will push it onto your mobile device.

### API Reference ###

##### GameManager.cs #####
Handles navigation, battle and store functionaility.

##### UIManager.cs #####
Contains the methods for displaying UI events and the methods for the various button presses.

##### UIAnimator.cs #####
Contains methods for animating UI elements.

##### SaveLoadManager.cs #####
Contains methods for saving, loading and deleted save files.

##### SoundManager.cs #####
Contains methods for playing sounds.

##### JSONReader.cs #####
Uses Unity's inbuilt JSON functionaility to get data from the JSON files and convert them into C# objects. Contains all 

##### Object scripts (Weapon.cs, WalkEvent.cs, etc) #####
These contain the spefic variables needed to be able to have JSON converted into it.


### Who do I talk to? ###

Shaun Anderson
shaun.anderson@outlook.com
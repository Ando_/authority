Images:
All images were created by me.

Sounds:
TypewriterSound - CC0 1.0 Universal (CC0 1.0) <https://freesound.org/people/cabled_mess/sounds/360602/>
PressSound - CC0 1.0 Universal (CC0 1.0) <https://freesound.org/people/OwlStorm/sounds/404792/>

Font:
Comfortaa - SIL Open Font License, Version 1.1. <https://www.dafont.com/comfortaa.font>
# Change Log #

### 1st April - 17th 2018 ###
- Created project.
- Created basic UI.

### 18th April 2018 ###

Rebuilt whole project to account for the new direction I am taking it:

- Built new layout.
- Deleted some unneeded files from the old project.
- Added some functionaility (Clock system).

### 19th April 2018 ###

Just removed a few more unneeded files.

### 20th April 2018 ###

Continued removing unneeded files and Updating functionaility.

- Removed TextMeshPro asset.
- Create save data object.

### 21th April 2018 ###

Added new functionaility and data.

- Added new JSON files and finilised how the data will be stored.
- Added functionaility to show event UI when needed.

### 22nd April 2018 ###

- Created the travel panel.

### 23rd April 2018 ###

- Updated UI layouts for both the store panel and the event panel.

### 24th April 2018 ###

Finilised on the end point of the project

- Created new json files.
- Changed project name.
- Finished the dynamic button methods.
- Added basic store functionaility.

### 25th - 29th April 2018 ###

Holidays

### 30th April 2018 ###

Added Basic battle UI and have implement functionaility to display it when needed.

- Batlle UI added.
- Buttons now appear red when leading to battles.
- The battle UI is now shown when needed.
- Updated JSON event file.


### 1st - 4th May 2018 ###

Was sick for most of this so just wrote down some new encounters in my exercise book.

### 5th May 2018 ###

Save functionaility added.

- Can save data.
- Multiple save files.

### 6th May 2018 ###

Updated layouts and create a new menu system.

- Continued developing load functionailty.
- Created new menu layout.

### 7th May 2018 ###

Added basic store layout functionailty.

- Added store display methods.
- Added Icon.

### 8th May 2018 ###

Expanded json files with more encounters.

### 9th May 2018 ###

Added new encounters and changed the layout of the Main scene.

### 10th May 2018 ###

Added new stat UI and methods.

### 11th May 2018 ###

- Added new methods to interact with the new stat UI.
- Created new battle and event UI.
- Create new progression loop (Room based now instead of time based).
- Added new action button.
- Expanded the JSON files for new events.

### 12th May 2018 ###

- Updated how stat changes are displayed.
- Updated layouts.
- Created new inventory screen.

### 13th May 2018 ###
- Added inventory functionaility.
- Added new json and object files for weapons and other items.

### 14th May 2018 ###
- Added iand expanded JSON Files.
- Updated layout.

### 15th May 2018 ###
- Expanded JSON Files.
- Added ability to add items through JSON file.

### 16th May 2018 ###
- Expanded JSON Files.
- Changed file layout.
- Updated UI layouts.

### 17th May 2018 ###
- Expanded JSON Files.
- Did demonstration video
- Cleaned Layout
- Created licenses file.
- Finshed battle system.
- Started publishing process.

### 18th May 2018 ###
- Expanded JSON Files.
- Cleaned Layout
- Updated licenses file.
- Finished publishing process.
